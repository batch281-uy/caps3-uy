import { Row, Col, Card, Button } from "react-bootstrap";
import { Placeholder } from "react-bootstrap";
export default function Highlights() {
  return (
    // <Row className="mt-3 mb-3">
    //   <Col xs={12} md={4}>
    //     <Card className="cardHighlight p-3">
    //       <div id="toysImg"></div>
    //       <Card.Body>
    //         <Card.Title>
    //           <h2>Fishing Rods</h2>
    //         </Card.Title>
    //         <Card.Text>
    //           Introducing our premium fishing rod, designed to enhance your
    //           angling experience and reel in unforgettable catches. Crafted with
    //           meticulous attention to detail and utilizing cutting-edge
    //           materials, this fishing rod combines strength, sensitivity, and
    //           versatility, making it an ideal companion for anglers of all skill
    //           levels.
    //         </Card.Text>
    //       </Card.Body>
    //     </Card>
    //   </Col>

    //   <Col xs={12} md={4}>
    //     <Card className="cardHighlight p-3" id="treatsImg">
    //       <Card.Body>
    //         <Card.Title>
    //           <h2>Fishing Reels</h2>
    //         </Card.Title>
    //         <Card.Text>
    //           Introducing our high-performance fishing reel, engineered to
    //           elevate your fishing experience to new heights. Meticulously
    //           designed and packed with advanced features, this reel is a
    //           game-changer in the world of angling. From its robust construction
    //           to its smooth operation, every aspect has been optimized to
    //           deliver unparalleled performance.
    //         </Card.Text>
    //       </Card.Body>
    //     </Card>
    //   </Col>

    //   <Col xs={12} md={4}>
    //     <Card className="cardHighlight p-3" id="gearsImg">
    //       <Card.Body>
    //         <Card.Title>
    //           <h2>Jigs and Lures</h2>
    //         </Card.Title>
    //         <Card.Text>
    //           Introducing our collection of premium fishing lures and jigs,
    //           meticulously crafted to entice and outsmart even the most elusive
    //           fish species. Designed by avid anglers for anglers, these lures
    //           are the ultimate tools for enhancing your fishing success and
    //           reeling in trophy catches. Each lure and jig is carefully
    //           engineered to mimic natural prey, provoking instinctive strikes
    //           and maximizing your chances of landing the big one.
    //         </Card.Text>
    //       </Card.Body>
    //     </Card>
    //   </Col>
    // </Row>

    <div className="d-flex justify-content-around">
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://images.pexels.com/photos/8305284/pexels-photo-8305284.jpeg?auto=compress&cs=tinysrgb&w=600"
        />
        <Card.Body>
          <Card.Title>Attention!</Card.Title>
          <Card.Text>
            The general's coffee philosophy is to ensure the quality of the
            coffee beans we deliver to you are fresh from the grinder. No time
            wasted. Attention to details from packing to delivery to ensure
            freshly roasted coffee is delivered at your doorsteps.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://images.pexels.com/photos/7125437/pexels-photo-7125437.jpeg?auto=compress&cs=tinysrgb&w=600"
        />
        <Card.Body>
          <Card.Title>Proudly Homegrown</Card.Title>
          <Card.Text>
            We take pride in sourcing our products from all over the
            Philippines. This is how we support our local farmers. We appreciate
            the love they put in their work and in the land they nourish.
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://images.pexels.com/photos/3280130/pexels-photo-3280130.jpeg?auto=compress&cs=tinysrgb&w=600"
        />
        <Card.Body>
          <Card.Title>
            <h2>Be Part of Our Community</h2>
          </Card.Title>
          <Card.Text>
            Join us as we help bridge the gaps between our local farmers and the
            rest of the world. Let the world know the best of the Philippines.
            Mabuhay!
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}
