import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import ProductView from "./components/ProductView";
import Products from "./pages/Products";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Profile from "./pages/Profile";
import AdminPortal from "./pages/AdminPortal";
import "./App.css";
import { UserProvider } from "./UserContext";
import bgImg from "./video/bg.mp4";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details/:userId`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      });
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container className="App">
            <video autoPlay loop muted>
              <source src={bgImg} type="video/mp4" />
            </video>

            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/products" element={<Products />} />
              <Route exact path="/productView/:id" element={<ProductView />} />
              <Route exact path="/register" element={<Register />} />
              <Route exact path="/login" element={<Login />} />
              <Route exact path="/logout" element={<Logout />} />
              <Route exact path="/profile" element={<Profile />} />
              <Route exact path="/adminPortal" element={<AdminPortal />} />
              <Route exact path="/getAllProducts" element={<AdminPortal />} />

              <Route exact path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
